package logCLI

// Error will print the log to stderr and return an indented log
func (l *LogCLI) Error(s string, params ...interface{}) *LogCLI {
	return l.baseLogCLI(errorPrefix, s, params...)
}
