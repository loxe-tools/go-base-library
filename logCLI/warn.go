package logCLI

// Warn will print the log to stdout and return an indented log
func (l *LogCLI) Warn(s string, params ...interface{}) *LogCLI {
	return l.baseLogCLI(warnPrefix, s, params...)
}
