package logCLI

// NewLogCLI will create a new CLI logger
//
// The debug param is used to control whether
// debug logs are printed to stdout or not
func NewLogCLI(debug, supportsANSI bool) *LogCLI {
	newLog := &LogCLI{
		indent: 0,
		rootLog: &rootLog{
			supportsANSI:      supportsANSI,
			debug:             debug,
			currentLineNumber: 0,
		},
		parent: nil,
	}
	newLog.Debug("New LogCLI created")
	return newLog
}

// LogCLI holds information about
// indentation of the log and if
// debugs log should be printed or
// not
type LogCLI struct {
	indent  int
	rootLog *rootLog
	parent  *parentLogInfo
}

type rootLog struct {
	supportsANSI      bool
	debug             bool
	currentLineNumber int
}

type parentLogInfo struct {
	lineNumber int
	prefix     string
}
