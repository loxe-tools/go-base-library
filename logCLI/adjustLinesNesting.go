package logCLI

import (
	"fmt"
)

func adjustLinesNesting(prefix string, indentation, vOffset int) {
	for i := 0; i < vOffset; i++ {
		fmt.Printf("\033[%dF", 1) // ANSI code CSI n F (CPL - Cursor Previous Line): Go to the beginning of the line "n" lines up
		hOffset := indentation - indentSize
		if hOffset > 0 {
			fmt.Printf("\033[%dC", hOffset) // ANSI code CSI n C (CUF - Cursor Forward): Forwards the cursor "n" cells in the current line
		}
		fmt.Printf(colourizeByPrefix(prefix, "|"))
	}
	if vOffset > 0 {
		fmt.Printf("\033[%dE", vOffset) // ANSI code CSI n E (CPL - Cursor Next Line): Go to the beginning of the line "n" lines down
	}
}
