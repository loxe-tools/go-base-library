package logCLI

import (
	"os"
)

// Fatal will print to the stderr and panic
func (l *LogCLI) Fatal(s string, params ...interface{}) {
	l.baseLogCLI(fatalPrefix, s, params...)
	os.Exit(1)
}
