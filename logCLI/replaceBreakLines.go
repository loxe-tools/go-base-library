package logCLI

import "strings"

func replaceBreakLines(msg string, indentation int) (string, int) {
	linesUsed := strings.Count(msg, "\n") + 1
	if linesUsed > 1 {
		newBreakLine := "\n" + strings.Repeat(defaultIndentation, (indentation+indentSize)/indentSize)
		msg = strings.ReplaceAll(msg, "\n", newBreakLine)
	}

	return msg + "\n", linesUsed
}
