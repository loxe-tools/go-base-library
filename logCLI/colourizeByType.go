package logCLI

import "gitlab.com/loxe-tools/go-base-library/util"

func colourizeByPrefix(type_, msg string) string {
	switch type_ {
	case debugPrefix:
		return util.WhiteString(msg)
	case infoPrefix:
		return util.CyanString(msg)
	case warnPrefix:
		return util.YellowString(msg)
	case errorPrefix:
		return util.RedString(msg)
	case fatalPrefix:
		return util.BoldRedString(msg)
	default:
		return msg
	}
}
