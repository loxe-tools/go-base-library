package logCLI

import (
	"fmt"
	"strings"
)

// These are the strings that can be
// used to prefix the logs.
//
// Note that it is not type safe (it
// is just a string)
const (
	debugPrefix = "[ DEBUG ] "
	infoPrefix  = "[ INFO  ] "
	warnPrefix  = "[ WARN  ] "
	errorPrefix = "[ ERROR ] "
	fatalPrefix = "[ FATAL ] "
)

// baseLogCLI will print the result of the concatenation of the current indentation,
// plus the given prefix, plus the returned value of calling the Sprintf function
// with the 'format' and 'params' parameters.
//
// Any "\n" (line break) will be override in a way that the line will keep indented
//
// Note that the function can print the log to the stdout or stderr.
func (l *LogCLI) baseLogCLI(prefix, format string, params ...interface{}) *LogCLI {
	// TODO: limpar qualquer codigo ANSI da mensagem
	msgPrefix := strings.Repeat(defaultIndentation, l.indent/indentSize)
	if l.parent != nil && l.rootLog.supportsANSI {
		msgPrefix = strings.TrimSuffix(msgPrefix, defaultIndentation) + colourizeByPrefix(l.parent.prefix, nestedIndentation)
	}

	msg := fmt.Sprintf(prefix+format, params...)
	msg, linesUsed := replaceBreakLines(msg, l.indent)
	l.rootLog.currentLineNumber += linesUsed
	if l.rootLog.supportsANSI {
		if l.indent > 0 {
			vOffset := l.rootLog.currentLineNumber - l.parent.lineNumber - linesUsed - 1
			adjustLinesNesting(l.parent.prefix, l.indent, vOffset)
		}
		msg = colourizeByPrefix(prefix, msg)
	}

	fmt.Print(msgPrefix + msg)
	return &LogCLI{
		indent:  l.indent + indentSize,
		rootLog: l.rootLog,
		parent: &parentLogInfo{
			lineNumber: l.rootLog.currentLineNumber - linesUsed,
			prefix:     prefix,
		},
	}
}

// indentSize is the default indentation size
// to use when nesting logs
const indentSize = 4

var defaultIndentation = strings.Repeat(" ", indentSize)
var nestedIndentation = "`->" + strings.Repeat(" ", indentSize-3)
