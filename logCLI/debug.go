package logCLI

// Debug will print to the stdout with the debug prefix.
//
// Note that if the debug boolean is set to false, nothing
// will be printed and a log with the same indentation size
// will be returned
func (l *LogCLI) Debug(s string, params ...interface{}) *LogCLI {
	if l.rootLog.debug {
		return l.baseLogCLI(debugPrefix, s, params...)
	}

	return &LogCLI{
		indent:  l.indent,
		rootLog: l.rootLog,
		parent:  l.parent,
	}
}
