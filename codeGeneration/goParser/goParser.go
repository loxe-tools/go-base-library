package goParser

import (
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"go/token"
	"golang.org/x/tools/go/packages"
)

// GoParser is a type that can parse
// GO source code and iterate over the
// parsed code
type GoParser struct {
	// pkgs is a slice that contains all the
	// parsed packages
	pkgs []*packages.Package

	log     *logCLI.LogCLI
	fileSet *token.FileSet
	focus   *ParserFocus
}

// NewGoParser creates a new parser for GO source files
func NewGoParser(pattern string, config Config, log logCLI.LogCLI) (*GoParser, error) {
	printFinalConfig(pattern, config, &log)

	packagesLoadConfig := packagesLoadConfig(config, &log)
	pkgs, e := packages.Load(packagesLoadConfig, pattern)
	if e != nil {
		return nil, e
	}

	return &GoParser{
		pkgs:    pkgs,
		log:     &log,
		fileSet: packagesLoadConfig.Fset,
		focus:   config.Focus,
	}, nil
}
