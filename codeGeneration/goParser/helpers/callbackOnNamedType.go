package helpers

import (
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"go/types"
)

func CallbackOnNamedType(fieldType types.Type, callback func(obj *types.Named), log *logCLI.LogCLI) {
	switch type_ := fieldType.(type) {
	case *types.Basic:
		return
	case *types.Pointer:
		CallbackOnNamedType(type_.Elem(), callback, log)
	case *types.Array:
		CallbackOnNamedType(type_.Elem(), callback, log)
	case *types.Slice:
		CallbackOnNamedType(type_.Elem(), callback, log)
	case *types.Map:
		CallbackOnNamedType(type_.Elem(), callback, log)
		CallbackOnNamedType(type_.Key(), callback, log)
	case *types.Chan:
		CallbackOnNamedType(type_.Elem(), callback, log)
	case *types.Struct:
		for i := 0; i < type_.NumFields(); i++ {
			CallbackOnNamedType(type_.Field(i).Type(), callback, log)
		}
	case *types.Tuple:
		for i := 0; i < type_.Len(); i++ {
			CallbackOnNamedType(type_.At(i).Type(), callback, log)
		}
	case *types.Signature:
		CallbackOnNamedType(type_.Params(), callback, log)
		CallbackOnNamedType(type_.Results(), callback, log)
	case *types.Named:
		callback(type_)
	case *types.Interface:
		for i := 0; i < type_.NumMethods(); i++ {
			CallbackOnNamedType(type_.Method(i).Type(), callback, log)
		}
	default:
		log.Fatal("unexpected field type: %s", type_.String())
	}
}
