package brand

import (
	"fmt"
	"strings"
)

// WithSubtitle will return the ASCII art with Title
// and given subtitle truncated (if necessary) and
// centralized
func WithSubtitle(subtitle string) string {
	return fmt.Sprintf(
		"%s%s%s", brandAsciiArt, truncateAndCentralize(brandTitle), truncateAndCentralize(subtitle))
}

// truncateAndCentralize will truncate the given
// string (if necessary) and add white spaces to
// it as a prefix.
//
// The white spaces are used to centralize the
// text with ASCII art as a reference
func truncateAndCentralize(txt string) string {
	if txt == "" {
		return ""
	}
	if len(txt) > brandAsciiArtLineLen {
		return fmt.Sprintf(
			"%s\n%s", txt[0:brandAsciiArtLineLen], truncateAndCentralize(txt[brandAsciiArtLineLen:]))
	}
	return fmt.Sprintf("%s%s\n",
		strings.Repeat(" ", (brandAsciiArtLineLen-len(txt))/2),
		txt,
	)
}
