package brand

// GeneratedFileSuffix is the suffix that must
// be present in the name of any file generated
// by a Löxe tool
const GeneratedFileSuffix = ".loxe"

// GeneratedFileMessage is the default message that
// must be displayed inside every file that any Löxe
// tool generates
//
// Note that it doesn't have any library name as a suffix,
// because it is dynamic and can't be predicted at compile
// time. Append the library name AND version to this string
const GeneratedFileMessage = "FILE GENERATED AUTOMATICALLY BY "
